import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import { MainComponent } from './components/main/main.component';
import {RouterLink, RouterOutlet} from "@angular/router";
import { PresentationCardComponent } from './components/common/presentation-card/presentation-card.component';
import {MatCardModule} from "@angular/material/card";
import {MatListModule} from "@angular/material/list";
import {MatChipsModule} from "@angular/material/chips";
import {MatTooltipModule} from "@angular/material/tooltip";
import { AddCartDialogComponent } from './components/common/add-cart-dialog/add-cart-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {CdkTableModule} from "@angular/cdk/table";


@NgModule({
  declarations: [
    HeaderComponent,
    MainComponent,
    PresentationCardComponent,
    AddCartDialogComponent,
  ],
  exports: [
    HeaderComponent,
    MainComponent,
    PresentationCardComponent,
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatTooltipModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    RouterOutlet,
    RouterLink,
    MatCardModule,
    MatListModule,
    MatChipsModule,
    MatDialogModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    CdkTableModule
  ]
})
export class CoreModule { }

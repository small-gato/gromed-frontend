import {Presentation} from "../../../api/gromed";

export interface CartAddData {
  presentation: Presentation;
  quantity: number;
}

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl} from "@angular/forms";
import {CartAddData} from "./cart-add.data";

@Component({
  selector: 'app-add-cart-dialog',
  templateUrl: './add-cart-dialog.component.html',
  styleUrls: ['./add-cart-dialog.component.scss']
})
export class AddCartDialogComponent {
  formControl = new FormControl('1');
  constructor(public dialogRef: MatDialogRef<AddCartDialogComponent>,@Inject(MAT_DIALOG_DATA) public data: CartAddData) {
    this.formControl.valueChanges.subscribe(value => {
      if (value !== null) this.data.quantity = Number.parseInt(value)
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  decrement() {
    if (this.data.quantity > 1) {
      this.data.quantity--;
      this.formControl.setValue(this.data.quantity.toString(10));
    }
  }

  increment() {
    this.data.quantity++;
    this.formControl.setValue(this.data.quantity.toString(10));
  }
}

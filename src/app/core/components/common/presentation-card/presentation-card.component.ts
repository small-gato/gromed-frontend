import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Presentation} from "../../../api/gromed";
import {CartAddData} from "../add-cart-dialog/cart-add.data";
import {AddCartDialogComponent} from "../add-cart-dialog/add-cart-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-presentation-card',
  templateUrl: './presentation-card.component.html',
  styleUrls: ['./presentation-card.component.scss']
})
export class PresentationCardComponent {
  @Input()
  presentation!: Presentation;
  @Output()
  cardClicked = new EventEmitter<CartAddData>();

  constructor(public dialog: MatDialog) {
  }

  addToCart() {
    const dialogRef = this.dialog.open(AddCartDialogComponent, {
      data: {presentation: this.presentation, quantity: 1},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.cardClicked.emit(result);
    });
  }
}

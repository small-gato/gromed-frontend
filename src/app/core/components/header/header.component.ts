import { Component } from '@angular/core';
import {AuthService} from "../../../auth/services/auth.service";
import {User} from "../../api/gromed";

@Component({
  selector: 'core-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(private authService:AuthService) {
  }
  get authUserObs(){
    return this.authService.authUserObs;
  }
  logout(){
    this.authService.logout();
  }

  isUser(user: User) {
    return this.authService.isUser(user);
  }
}

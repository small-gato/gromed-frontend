/**
 * Gromed backend
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Prescription } from './prescription';


export interface Medicament { 
    codeCIS?: number;
    nomMedicament?: string;
    formePharmacetique?: string;
    etatComercialisation?: string;
    lienWeb?: string;
    fabricant?: string;
    numeroASMR?: string;
    libelleASMR?: string;
    prescriptions?: Set<Prescription>;
    groupes?: Set<object>;
    substances?: Set<object>;
}


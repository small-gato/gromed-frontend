import {Component} from '@angular/core';
import {DetailsPresentationService} from "../../services/detailsPresentation.service";
import {Presentation} from "../../../core/api/gromed";
import {ActivatedRoute} from "@angular/router";
import {CartAddData} from "../../../core/components/common/add-cart-dialog/cart-add.data";
import {FormControl} from "@angular/forms";
import {mergeMap, Observable, switchMap} from "rxjs";
import {CartService} from "../../../cart/services/cart.service";


@Component({
  selector: 'app-detail-produit',
  templateUrl: './detail-produit.component.html',
  styleUrls: ['./detail-produit.component.scss']
})
export class DetailProduitComponent {
  formControl = new FormControl('1');
  data: CartAddData = {quantity:1,presentation:{}};
  constructor(private detailsPresentationService : DetailsPresentationService, private route: ActivatedRoute, private cart :CartService) {
    this.formControl.valueChanges.subscribe(value => {
      if (value !== null) this.data.quantity = Number.parseInt(value)
    })
    this.presentation = this.route.params.pipe(switchMap(v=>this.detailsPresentationService.getPresentationDetails(Number.parseInt(v['id']))));
    this.prescriptionsDeMedicaments = this.presentation.pipe(switchMap((presentation)=>{
      return presentation!.medicaments!.map(med=>this.detailsPresentationService.getPrescriptionsByCodeCIS(med!.codeCIS!))
    }),mergeMap(value => value));
    this.presentation.subscribe(v=>this.data.presentation=v)
}
  presentation:Observable<Presentation>;
  prescriptionsDeMedicaments: Observable<Array<String>> ;
  checked:boolean=false;

  addToCart() {
        this.cart.addItemToCart(this.data.presentation.codeCip!,this.data.quantity)
    }

  decrement() {
    if (this.data.quantity > 1) {
      this.data.quantity--;
      this.formControl.setValue(this.data.quantity.toString(10));
    }
  }

  increment() {
    this.data.quantity++;
    this.formControl.setValue(this.data.quantity.toString(10));
  }
  }


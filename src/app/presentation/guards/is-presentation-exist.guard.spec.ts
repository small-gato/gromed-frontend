import { TestBed } from '@angular/core/testing';

import { IsPresentationExistGuard } from './is-presentation-exist.guard';

describe('IsPresentationExistGuard', () => {
  let guard: IsPresentationExistGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(IsPresentationExistGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {PresentationControllerService} from "../../core/api/gromed";
import {AuthService} from "../../auth/services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class DetailsPresentationService {

  constructor(private  authService : AuthService, private presentationService : PresentationControllerService) {
    this.authService.jwtTokenObsConfig.subscribe(data => presentationService.configuration=data);
  }
  getPresentationDetails(codeCIP : number){
    return this.presentationService.getPresentation(codeCIP);
  }

  getPrescriptionsByCodeCIS(codeCIS: number){
    return this.presentationService.getPrescriptions(codeCIS);
  }

  getGeneriqueOfPresentation(codeCIS: number){
    console.log("getGeneriqueOfPresentation("+codeCIS+")")
    return this.presentationService.getPresentationsSameGroup(codeCIS);
  }

}

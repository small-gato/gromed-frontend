import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PresentationRoutingModule} from "./presentation-routing.module";
import { PresentationContainerComponent } from './containers/presentation-container/presentation-container.component';
import { DetailProduitComponent } from './components/detail-produit/detail-produit.component';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {CoreModule} from "../core/core.module";


@NgModule({
  declarations: [
    PresentationContainerComponent,
    DetailProduitComponent
  ],
    imports: [
        CommonModule,
        MatCardModule,
        PresentationRoutingModule,
        MatIconModule,
        MatTooltipModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatListModule,
        MatCheckboxModule,
        FormsModule,
        CoreModule
    ]
})
export class PresentationModule { }

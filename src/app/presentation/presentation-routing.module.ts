import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {PresentationContainerComponent} from "./containers/presentation-container/presentation-container.component";
import {IsPresentationExistGuard} from "./guards/is-presentation-exist.guard";


const routes: Routes = [
  {
    path: ':id',
    component: PresentationContainerComponent,
    canActivate: [IsPresentationExistGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PresentationRoutingModule {
}

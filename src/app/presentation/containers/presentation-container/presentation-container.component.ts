import { Component } from '@angular/core';
import {DetailsPresentationService} from "../../services/detailsPresentation.service";
import {switchMap} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {CartAddData} from "../../../core/components/common/add-cart-dialog/cart-add.data";
import {CartService} from "../../../cart/services/cart.service";

@Component({
  selector: 'app-presentation-container',
  templateUrl: './presentation-container.component.html',
  styleUrls: ['./presentation-container.component.scss']
})
export class PresentationContainerComponent {

  generiquePreseObservable = this.route.params.pipe(
    switchMap(v=>this.detailsPresentationService.getPresentationDetails(Number.parseInt(v['id']))),
    switchMap(v=>this.detailsPresentationService.getGeneriqueOfPresentation(v.medicaments![0].codeCIS!))
  );

  constructor(private detailsPresentationService:DetailsPresentationService,
              private cartService: CartService,
              private route: ActivatedRoute){
  }

  ajouterAuPanier(event: CartAddData) {
    this.cartService.addItemToCart(event.presentation!.codeCip!,event.quantity);
  }
}

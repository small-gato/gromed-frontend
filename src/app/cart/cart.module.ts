import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {MatCardModule} from "@angular/material/card";
import { CartRoutingModule } from './cart-routing.module';
import {CartComponent} from "./container/cart.component";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { CartContentComponent } from './container/cart-content/cart-content.component';
import { CartRecapComponent } from './container/cart-recap/cart-recap.component';
import {MatExpansionModule} from "@angular/material/expansion";
import { CartAnnulationComponent } from './container/cart-annulation/cart-annulation.component';
import { QuantityEditComponent } from './container/quantity-edit/quantity-edit.component';
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatListModule} from "@angular/material/list";
import {MatSnackBarModule} from "@angular/material/snack-bar";


@NgModule({
  declarations: [
    CartComponent,
    CartContentComponent,
    CartRecapComponent,
    CartAnnulationComponent,
    QuantityEditComponent,
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatDialogModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatInputModule,
    ReactiveFormsModule,
    MatListModule,
    MatSnackBarModule
  ]
})
export class CartModule { }

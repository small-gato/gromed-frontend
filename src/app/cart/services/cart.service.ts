import { Injectable } from '@angular/core';
import {AuthService} from "../../auth/services/auth.service";
import {CommandControllerService, Commande, ProduitCommande} from "../../core/api/gromed";
import {BehaviorSubject, catchError, firstValueFrom, map, Observable} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private cartSubject = new BehaviorSubject<ProduitCommande[]>([]);
  private autoUpdateInterval: any;
  constructor(private authService : AuthService,private panierService:CommandControllerService,private matSnackBar:MatSnackBar) {
    this.authService.jwtTokenObsConfig.subscribe(v=> {
      panierService.configuration = v;
      this.panierService.getPanier().subscribe(value => this.cartSubject.next([...value.produitCommandes!]))
    });
  }

  public async addItemToCart(codeCip : number,quantity:number){
    await firstValueFrom(this.panierService.addPresentation(codeCip,quantity));
    this.matSnackBar.open("Produit ajouté au panier", "OK", {duration: 3000,panelClass: ['success-snackbar']});
    this.updateCart()
  }

  public getCart(): Observable<Commande>{
    return this.panierService.getPanier();
  }

   public  getCartProducts(){
    return this.cartSubject.asObservable();
   }

   public updateCart(){
     this.panierService.getPanier().subscribe(value => this.cartSubject.next([...value.produitCommandes!]))
   }

   public async validateCart() {
     await firstValueFrom(this.panierService.validerPanier());
     this.matSnackBar.open("Commande validée avec succès", "OK", {duration: 3000,panelClass: ['success-snackbar']});
     this.updateCart();
   }

   public async editQuantity(codeCip: number, quantite: number) {
    await firstValueFrom(this.panierService.editPresentation(codeCip, quantite));
    this.matSnackBar.open("Quantité modifiée avec succès", "OK", {duration: 3000,panelClass: ['success-snackbar']});
    this.updateCart();
   }

   public cancelCart(idCommande: number) {
      return this.panierService.annulerCommande(idCommande).pipe(map(()=> {
        this.matSnackBar.open("Commande annulée avec succès", "OK", {duration: 3000,panelClass: ['success-snackbar']});
        return true
      }),catchError(async () => {
        this.matSnackBar.open("Erreur lors de l'annulation de la commande", "OK", {duration: 3000,panelClass: ['error-snackbar']});
        return false
      }));
   }

  autoUpdateStart() {
    console.log("start");
    this.autoUpdateInterval = setInterval(() => {
      this.updateCart();
    },1000);
  }

  autoUpdateStop() {
    console.log("stop");
    clearInterval(this.autoUpdateInterval);
  }
}

import {Component, EventEmitter, Inject, Input, Output} from '@angular/core';
import {ProduitCommande} from "../../../core/api/gromed";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CartAddData} from "../../../core/components/common/add-cart-dialog/cart-add.data";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-quantity-edit',
  templateUrl: './quantity-edit.component.html',
  styleUrls: ['./quantity-edit.component.scss']
})
export class QuantityEditComponent {

  @Input() product!: ProduitCommande;
  @Output() edit = new EventEmitter<{code: number, quantity: number}>()

  @Input() quantite!: number;

  constructor() {
  }

  public decrement() {
    if (this.quantite! > 1) {
      this.quantite!--;
      this.edit.emit({code: this.product.presentation!.codeCip!, quantity: this.quantite!});
    }
  }

  public increment() {
    this.quantite!++;
    this.edit.emit({code: this.product.presentation!.codeCip!, quantity: this.quantite!});
  }

  public onChange(quantity: string) {
    this.quantite = Number(quantity);
    this.edit.emit({code: this.product.presentation!.codeCip!, quantity: this.quantite!});
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartAnnulationComponent } from './cart-annulation.component';

describe('CartAnnulationComponent', () => {
  let component: CartAnnulationComponent;
  let fixture: ComponentFixture<CartAnnulationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartAnnulationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CartAnnulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Commande} from "../../../core/api/gromed";

@Component({
  selector: 'app-cart-annulation',
  templateUrl: './cart-annulation.component.html',
  styleUrls: ['./cart-annulation.component.scss']
})
export class CartAnnulationComponent {

  @Input() commande!: Commande;
  @Output() step = new EventEmitter<number>();
  @Output() commandBack = new EventEmitter<void>();
  @Output() update = new EventEmitter<void>();

  public emitStep(step: number){
    this.step.emit(step);
  }

  constructor() {
  }

}

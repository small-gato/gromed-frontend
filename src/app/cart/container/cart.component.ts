import {Component} from '@angular/core';
import {CartService} from "../services/cart.service";
import {Commande, ProduitCommande} from "../../core/api/gromed";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent {

  public products: ProduitCommande[] = [];
  public commande: Commande = {};
  public step = 0;

  constructor(private cartService: CartService) {
    this.cartService.getCartProducts().subscribe(v=>this.products= v.sort((a,b) => a.presentation!.medicaments![0].nomMedicament!.localeCompare(b.presentation!.medicaments![0].nomMedicament!)));
    this.cartService.getCart().subscribe(c => this.commande=c);
    this.step = 0;
  }

  public hasProducts(): boolean {
    return this.products.length>0;
  }

  public setStep(step: number) : void {
    this.step = step;
    if (step === 1) this.cartService.autoUpdateStart();
    if (step !== 1) this.cartService.autoUpdateStop();
  }

  public getPorcentStep() : number{
    return (this.step/2)*100;
  }

  public updateCart(){
    this.cartService.updateCart();
  }

  changeQuantity(change: {code: number, quantity: number}) {
    this.cartService.editQuantity(change.code, change.quantity);
  }
}


import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ProduitCommande} from "../../../core/api/gromed";

@Component({
  selector: 'app-cart-content',
  templateUrl: './cart-content.component.html',
  styleUrls: ['./cart-content.component.scss']
})
export class CartContentComponent {

  @Input() products!: ProduitCommande[];
  @Output() step = new EventEmitter<number>();
  @Output() delete = new EventEmitter<number>();
  @Output() deleteAll = new EventEmitter<void>();

  @Output() edit = new EventEmitter<{code: number, quantity: number}>();

  constructor() {
  }

  public totalElementPrice(element: ProduitCommande): number {
    return element.presentation!.prix!* element.quantite!;
  }

  public totalPrice(products: ProduitCommande[]) : number{
    let prices = products.map(e => {
      return e.presentation!.prix! * e.quantite!;
    });
    return Math.round((prices.reduce((a,b) => {
      return a+b;
    }) + Number.EPSILON)*100)/100;
  }

  public emitStep(step: number){
    this.step.emit(step);
  }

  public emitEdit(change: {code: number, quantity: number}) {
    this.edit.emit({code: change.code, quantity: change.quantity});
  }
}

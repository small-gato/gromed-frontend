import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartRecapComponent } from './cart-recap.component';

describe('CartRecapComponent', () => {
  let component: CartRecapComponent;
  let fixture: ComponentFixture<CartRecapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartRecapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CartRecapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

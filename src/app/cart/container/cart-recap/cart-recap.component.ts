import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ProduitCommande} from "../../../core/api/gromed";
import {CartService} from "../../services/cart.service";

@Component({
  selector: 'app-cart-recap',
  templateUrl: './cart-recap.component.html',
  styleUrls: ['./cart-recap.component.scss']
})
export class CartRecapComponent {

  @Input() products!: ProduitCommande[];
  @Output() step = new EventEmitter<number>();
  public panelOpenState: boolean = false;

  constructor(private cartService: CartService) {
  }

  public totalPrice(products: ProduitCommande[]) : number{
    let prices = products.map(e => {
      return e.presentation!.prix! * e.quantite!;
    });
    return Math.round((prices.reduce((a,b) => {
      return a+b;
    }) + Number.EPSILON)*100)/100;
  }

  public emitStep(step: number){
    this.step.emit(step);
  }

  public totalElementPrice(element: ProduitCommande): number {
    return element.presentation!.prix! * element.quantite!;
  }

  public validateCart() : void {
    this.cartService.validateCart();
    this.emitStep(2);
  }

  public checkForStock(element: ProduitCommande): boolean {
    return element.quantite! <= element.presentation!.stockLogique!;
  }

  public checkForStockAll(products: ProduitCommande[]): boolean {
    console.log(products.every(e => this.checkForStock(e)))
    return products.every(e => this.checkForStock(e));
  }

}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {CommandsHistoryComponent} from "./containers/commands-history/commands-history.component";
import {CommandDisplayComponent} from "./containers/command-display/command-display.component";
import {IsCommandExistGuard} from "./guards/is-command-exist.guard";
import {CommandNotFoundComponent} from "./containers/command-not-found/command-not-found.component";


const routes: Routes = [
  {
    path: '',
    component:CommandsHistoryComponent
  },
  {
    path: ':id',
    component:CommandDisplayComponent,
    canActivate:[IsCommandExistGuard]
  },
  {
    path: 'not-found',
    component:CommandNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule {
}

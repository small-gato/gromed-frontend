import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HistoryRoutingModule} from "./history-routing.module";
import { CommandsHistoryComponent } from './containers/commands-history/commands-history.component';
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { CommandDisplayComponent } from './containers/command-display/command-display.component';
import { CommandNotFoundComponent } from './containers/command-not-found/command-not-found.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatCardModule} from "@angular/material/card";
import {MatListModule} from "@angular/material/list";
import {MatLineModule} from "@angular/material/core";
import {MatIconModule} from "@angular/material/icon";



@NgModule({
  declarations: [
    CommandsHistoryComponent,
    CommandDisplayComponent,
    CommandNotFoundComponent
  ],
  imports: [
    CommonModule,
    HistoryRoutingModule,
    MatTableModule,
    MatButtonModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatListModule,
    MatLineModule,
    MatIconModule
  ]
})
export class HistoryModule { }

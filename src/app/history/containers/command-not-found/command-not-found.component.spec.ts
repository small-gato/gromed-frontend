import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandNotFoundComponent } from './command-not-found.component';

describe('CommandNotFoundComponent', () => {
  let component: CommandNotFoundComponent;
  let fixture: ComponentFixture<CommandNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommandNotFoundComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CommandNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component } from '@angular/core';
import {HistoryStoreService} from "../../services/history-store.service";
import {Commande} from "../../../core/api/gromed";
import {ROWFADEIN} from "../../animations/row.animation";
import {CartService} from "../../../cart/services/cart.service";

@Component({
  selector: 'app-commands-history',
  templateUrl: './commands-history.component.html',
  styleUrls: ['./commands-history.component.scss'],
  animations: [ROWFADEIN]
})
export class CommandsHistoryComponent {

  commandsHistory:Commande[] = [];
  displayedColumns = ['identifiantCommande', 'dateValidation', 'commandeStatut', 'prix', 'actions']
  loading=true;
  constructor(private historyService:HistoryStoreService, private cartService:CartService) {
    this.historyService.getCommandsHistory().subscribe(commands=>{
      this.commandsHistory = commands;
      this.loading = false;
    });
  }

  getPrice(command:Commande){
    let price = 0;
    command.produitCommandes!.forEach(line=>{
      price += line.quantite! * line.presentation!.prix!;
    });
    return price.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' });
  }


  cancelCmd(element:Commande) {
    this.cartService.cancelCart(element.identifiantCommande!).subscribe((value)=>{
      if (value) {
        this.historyService.getCommandsHistory().subscribe(commands => {
          this.commandsHistory = commands;
        });
      }
    });
  }

  parseDate(dateValidation: string) {
    const date = new Date(dateValidation)
    // JJ/MM/AAAA HH:MM:SS
    return date.toLocaleDateString() + " " + date.toLocaleTimeString();
  }
}

import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Commande} from "../../../core/api/gromed";
import {HistoryStoreService} from "../../services/history-store.service";
import {Observable, of, switchMap} from "rxjs";
import {FADE} from "../../animations/fade.animation";
import {CartService} from "../../../cart/services/cart.service";

@Component({
  selector: 'app-command-display',
  templateUrl: './command-display.component.html',
  styleUrls: ['./command-display.component.scss'],
  animations: [FADE]
})
export class CommandDisplayComponent {
  commande: Observable<Commande>;
  loading = true;

  constructor(private route: ActivatedRoute,
              private historyStore: HistoryStoreService,
              private cartService:CartService,
              private router:Router) {
    //get id from route
    this.commande = this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('id')!;
        if (!isNaN(Number.parseInt(id))) {
          return this.historyStore.getCommandById(Number.parseInt(id));
        }
        return of({});
      }));
    this.commande.subscribe((param) => {
      console.log(param);
      this.loading = false
    });
  }

  calculeTotal(cmd: Commande) {
    let total = 0;
    cmd.produitCommandes!.forEach(l => {
      total += l.quantite! * l.presentation!.prix!;
    });
    return total.toLocaleString('fr-FR', {style: 'currency', currency: 'EUR'});
  }

  getLocalDate(s: string) {
    return new Date(s).toLocaleDateString('fr-FR', {timeZone: 'UTC'});
  }

  cancel(commande: Commande) {
    this.cartService.cancelCart(commande.identifiantCommande!).subscribe(async (value)=>{
      if (value){
        await this.router.navigate(['/history']);
      }
    })
  }
}

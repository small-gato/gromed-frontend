import {animate, query, style, transition, trigger} from "@angular/animations";

export const ROWFADEIN = trigger('rowAnimation', [
  transition('* => *', [
    query(':enter', style({opacity: 0})),
    query(':enter', animate('1s', style({opacity: 1})))
  ])
]);

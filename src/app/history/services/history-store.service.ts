import { Injectable } from '@angular/core';
import {AuthService} from "../../auth/services/auth.service";
import {CommandControllerService} from "../../core/api/gromed";
import {firstValueFrom} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HistoryStoreService {

  constructor(private auth: AuthService, private command:CommandControllerService) {
    this.auth.jwtTokenObsConfig.subscribe(config=>this.command.configuration = config);
  }

  getCommandsHistory(){
    return this.command.getHistoriqueCommand();
  }

  getCommandById(id: number) {
    return this.command.getCommand(id);
  }

  async checkIfCommandExist(id: number) {
    try {
      await firstValueFrom(this.command.checkIfCommandExist(id));
      return true;
    } catch (e) {
      return false;
    }
  }
}

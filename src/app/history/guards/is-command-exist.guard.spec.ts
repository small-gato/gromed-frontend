import { TestBed } from '@angular/core/testing';

import { IsCommandExistGuard } from './is-command-exist.guard';

describe('IsCommandExistGuard', () => {
  let guard: IsCommandExistGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(IsCommandExistGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

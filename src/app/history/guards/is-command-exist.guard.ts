import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {HistoryStoreService} from "../services/history-store.service";

@Injectable({
  providedIn: 'root'
})
export class IsCommandExistGuard implements CanActivate {

  constructor(private historyStore: HistoryStoreService, private router: Router) {
  }
  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    const id = route.paramMap.get('id');
    if (!id || isNaN(Number.parseInt(id))) {
      await this.router.navigate(['']);
      return false;
    }
    const command = await this.historyStore.checkIfCommandExist(Number.parseInt(id));
    if (!command) {
      await this.router.navigate(['']);
      return false;
    }
    return true;
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IsAuthenticatedGuard} from "./auth/guard/is-authenticated.guard";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'search',
  },
  {
    path: 'auth',
    loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule)
  },
  {
    path: 'search',
    loadChildren: () => import("./search/search.module").then(m => m.SearchModule),
    canActivate: [IsAuthenticatedGuard]
  },
  {
    path: 'history',
    loadChildren: () => import("./history/history.module").then(m => m.HistoryModule),
    canActivate: [IsAuthenticatedGuard],
  },
  {
    path:'cart',
    loadChildren: () => import("./cart/cart.module").then((m)=>m.CartModule),
    canActivate: [IsAuthenticatedGuard]
  },
  {
    path:'presentation',
    loadChildren: ()=> import("./presentation/presentation.module").then((m)=>m.PresentationModule),
    canActivate: [IsAuthenticatedGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import {Injectable} from '@angular/core';
import {
  Configuration,
  JwtTokenControllerService,
  Tokens,
  User
} from "../../core/api/gromed";
import {BehaviorSubject, firstValueFrom, map, Observable} from "rxjs";
import {AuthStoreService} from "./auth-store.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authUserSubjet = new BehaviorSubject<User>({});
  private jwtTokens = new BehaviorSubject<Tokens>({});

  get isLoggedIn(){
    return this.jwtTokens.asObservable().pipe(map(jwt => typeof jwt.jwtToken !== "undefined" && typeof jwt.refreshToken !== "undefined"));
  }

  constructor(private jwtToken: JwtTokenControllerService, private tokenStore: AuthStoreService, private  router: Router) {
    this.setupAutoReconect().then(() => console.log("auto reconect done"))
  }


  private async setupAutoReconect() {
    const tokens = this.tokenStore.getJwtTokens();
    if (tokens !== null) {
      try {
        await this.updateJwtTokenConnexion(tokens);
      }catch (e){
        try {
          const newTokens = await firstValueFrom(this.jwtToken.refreshTokens({refreshToken:tokens.refreshToken}))
          await this.updateJwtTokenConnexion(newTokens);
        }catch (e){
          this.tokenStore.removeJwtToken();
        }
      }
    }
  }

  public async login(login: string | null, pass: string | null): Promise<boolean> {
    this.jwtToken.configuration = new Configuration({
      basePath: this.jwtToken.configuration.basePath,
      encodeParam: this.jwtToken.configuration.encodeParam,
      encoder: this.jwtToken.configuration.encoder,
      withCredentials: true,
      username: login ? login : '',
      password: pass ? pass : '',
    });
    try {
      const tokens = await firstValueFrom(this.jwtToken.getTokens());
      await this.updateJwtTokenConnexion(tokens);
      this.router.navigate(['/'])
      return true;
    } catch (e) {
      console.log(e)
      return false;
    }
  }

  public async logout(){
    this.tokenStore.removeJwtToken();
    this.authUserSubjet.next({});
    this.jwtTokens.next({});
    this.router.navigate(['/auth/login'])
  }

  private async updateJwtTokenConnexion(tokens: Tokens) {
    this.tokenStore.setJwtToken(tokens);
    this.jwtToken.configuration = new Configuration({
      basePath: this.jwtToken.configuration.basePath,
      encodeParam: this.jwtToken.configuration.encodeParam,
      encoder: this.jwtToken.configuration.encoder,
      withCredentials: true,
      credentials: {
        ['bearerAuth']: tokens.jwtToken ? tokens.jwtToken : ''
      }
    });
    const user = await firstValueFrom(this.jwtToken.getUser())
    this.authUserSubjet.next(user);
    this.jwtTokens.next(tokens);
  }

  public get authUserObs(){
      return this.authUserSubjet.asObservable();
  }

  public get jwtTokenObsConfig():Observable<Configuration>{
    return this.jwtTokens.asObservable().pipe(map(v=>{
      return new Configuration({
        basePath: this.jwtToken.configuration.basePath,
        encodeParam: this.jwtToken.configuration.encodeParam,
        encoder: this.jwtToken.configuration.encoder,
        withCredentials: true,
        credentials: {
          ['bearerAuth']: v.jwtToken ? v.jwtToken : ''
        }
      });
    }));
  }

  isUser(user:User){
    return typeof user.email !== "undefined"
  }
}

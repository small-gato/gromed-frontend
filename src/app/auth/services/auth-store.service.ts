import { Injectable } from '@angular/core';
import {Tokens} from "../../core/api/gromed";

@Injectable({
  providedIn: 'root'
})
export class AuthStoreService {
  public getJwtTokens():Tokens|null{
    const jsonToken = localStorage.getItem("tokens");
    if(jsonToken===null){
      return null;
    }
    return JSON.parse(jsonToken) as Tokens;
  }

  public setJwtToken(tokens :Tokens){
    localStorage.setItem("tokens",JSON.stringify(tokens));
  }

  public removeJwtToken(){
    localStorage.removeItem("tokens")
  }
}

import { NgModule } from '@angular/core';
import { RegisterComponent } from './containers/register/register.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import { LoginContainer } from './containers/login/login-container.component';
import {AuthRoutingModule} from "./auth-routing.module";
import {CoreModule} from "../core/core.module";
import {AsyncPipe, CommonModule} from "@angular/common";



@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    AuthRoutingModule,
    CoreModule,
    AsyncPipe,
  ],
  exports: [
    RegisterComponent
  ],
  declarations: [
    RegisterComponent,
    LoginContainer
  ]
})
export class AuthModule { }

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LoginContainer} from "./containers/login/login-container.component";
import {RegisterComponent} from "./containers/register/register.component";


const routes: Routes = [
  {
    path: 'login',
    component: LoginContainer,
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}

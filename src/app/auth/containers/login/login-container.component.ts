import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {User} from "../../../core/api/gromed";

@Component({
  selector: 'app-login',
  templateUrl: './login-container.component.html',
  styleUrls: ['./login-container.component.scss']
})
export class LoginContainer {
  loginForm= new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required, Validators.maxLength(254)]),
    password: new FormControl('', [Validators.required])
  })

  constructor(private authService:AuthService) {
  }

  onSubmit(){
    this.authService.login(this.loginForm.controls['email'].value,this.loginForm.controls['password'].value)
  }

  logout(){
    this.authService.logout();
  }

  isUser(user:User){
    return typeof user.email !== "undefined"
  }

  checkError(control: 'email'|'password', error: string) {
    return this.loginForm.controls[control].hasError(error);
  }

  get authUserObs(){
    return this.authService.authUserObs;
  }
}

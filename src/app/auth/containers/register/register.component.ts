import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  registerForm = new FormGroup({
    email:new FormControl('',[Validators.email,Validators.required,Validators.maxLength(254)]),
    firstName:new FormControl('',[Validators.required,Validators.maxLength(254),Validators.minLength(3)]),
    lastName:new FormControl('',[Validators.required,Validators.maxLength(254),Validators.minLength(3)]),
    password:new FormControl('',[Validators.required,Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{16,}$/g)])
  })
}

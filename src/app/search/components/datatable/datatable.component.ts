import {Component, ViewChild} from '@angular/core';
import {PagePresentation} from "../../../core/api/gromed";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {SearchStoreService} from "../../services/search-store/search-store.service";
import {FADEINOUT} from "../../animations/card-fade-in-out.animation";
import {debounceTime} from "rxjs";
import {CartAddData} from "../../../core/components/common/add-cart-dialog/cart-add.data";
import {CartService} from "../../../cart/services/cart.service";

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  animations: [FADEINOUT],
  styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent {
  dataSource: PagePresentation = {content: [], totalElements: 0};
  totalLength: number=0;
  pageSize: number=8;
  pageIndex: number=0;
  pageOptions=[8,16,32,64];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  isLoading =false;

  constructor(private searchStore: SearchStoreService, private cartService: CartService) {
    this.searchStore.searchConfig.asObservable().subscribe(()=>{
      this.isLoading=true;
      this.dataSource = {content: [], totalElements: 0};
    });
    this.searchStore.presentationObs.pipe(debounceTime(500)).subscribe(data => {
      this.dataSource = data;
      this.totalLength = data.totalElements!;
      this.pageSize = data.size!;
      this.pageIndex = data.number!;
      this.isLoading=false;
    })
  }

  pageChanged(event: PageEvent) {
    this.pageSize=event.pageSize;
    this.pageIndex=event.pageIndex;
    this.searchStore.updatePageParams(event);
  }

  isUndefined(value: any) {
    return typeof value === 'undefined';
  }

  ajouterAuPanier(event: CartAddData) {
    this.cartService.addItemToCart(event.presentation!.codeCip!,event.quantity);
  }
}

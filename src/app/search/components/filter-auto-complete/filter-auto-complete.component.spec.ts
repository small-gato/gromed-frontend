import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterAutoCompleteComponent } from './filter-auto-complete.component';

describe('FilterAutoCompleteComponent', () => {
  let component: FilterAutoCompleteComponent;
  let fixture: ComponentFixture<FilterAutoCompleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterAutoCompleteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FilterAutoCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

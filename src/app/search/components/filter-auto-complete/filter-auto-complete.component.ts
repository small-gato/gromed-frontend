import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {BehaviorSubject, debounceTime, filter, map} from "rxjs";
import {MatChipEditedEvent, MatChipInputEvent} from "@angular/material/chips";
import {COMMA, ENTER, TAB} from "@angular/cdk/keycodes";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";

@Component({
  selector: 'app-filter-auto-complete',
  templateUrl: './filter-auto-complete.component.html',
  styleUrls: ['./filter-auto-complete.component.scss']
})
export class FilterAutoCompleteComponent {

  filters = new BehaviorSubject<string[]>([]);
  readonly separatorKeysCodes = [ENTER,COMMA,TAB] as const;
  filterControl = new FormControl('');
  @Input() title: string ='';
  @Input() addOnBlur= true;
  @Input() filteredOptions: string[] = [];
  @Output() changeVal = this.filterControl.valueChanges.pipe(
    filter(value => typeof value ==="string"),
    map(value => value as string),
    debounceTime(100),
  )
  @Output() selectedFilters = this.filters.asObservable();
  @Input() set selectedFiltersValue(value: string[]) {
    this.filters.next(value);
  }
  @Output() addFilter = new EventEmitter<string>();
  @Output() removeFilter = new EventEmitter<string>();
  @Output() editFilter = new EventEmitter<{source:string,value:string}>();
  @ViewChild("filterInput") filterInput!: ElementRef;



  remove(filtered: string) {
    this.filters.next(this.filters.value.filter(v=>v!==filtered));
    this.removeFilter.emit(filtered);
  }

  edit(filtered: string, $event: MatChipEditedEvent) {
    this.filters.next(this.filters.value.map(v=>v===filtered?$event.value:v));
    this.editFilter.emit({source:filtered,value:$event.value});
  }

  add($event: MatChipInputEvent) {
    const value = ($event.value || '').trim();
    if (value) {
      this.filters.next([...this.filters.value, value]);
      this.addFilter.emit(value);
    }
    $event.chipInput!.clear();
  }

  valueSelect(event: MatAutocompleteSelectedEvent) {
    this.add({chipInput:{clear:()=>{}},value:event.option.viewValue } as MatChipInputEvent);
    this.filterControl.setValue(null);
    this.filterInput.nativeElement.value = '';
  }
}

import {Injectable} from '@angular/core';
import {BehaviorSubject, debounceTime, map, Observable, switchMap} from "rxjs";
import {emptySearch, SearchParameters, TypeGen} from "./search.data";
import {PagePresentation, PresentationControllerService} from "../../../core/api/gromed";
import {PageEvent} from "@angular/material/paginator";
import {AuthService} from "../../../auth/services/auth.service";


@Injectable({
  providedIn: 'root'
})
export class SearchStoreService {

  searchConfig = new BehaviorSubject<SearchParameters>(emptySearch);
  _presentationObs: Observable<PagePresentation>;
  get presentationObs() {
    return this._presentationObs
  }

  constructor(private presentation: PresentationControllerService, private authService: AuthService) {
    this._presentationObs = this.authService.jwtTokenObsConfig.pipe(map(data => presentation.configuration = data),debounceTime(100), switchMap(() => {
      return this.searchConfig.asObservable().pipe(switchMap(search => this.searchValues(search)));
    }))
  }

  private searchValues(search: SearchParameters) {
    return this.presentation.searchPresentation(search.page, search.itemPerPage, search.sort, search.direction, search.fabriquants, search.formes, search.typeGens, search.substances, search.medicament);
  }

  updateTypeGen(typeGen: TypeGen[]) {
    const search = this.searchConfig.getValue();
    search.typeGens = typeGen;
    this.searchConfig.next(search);
  }

  updatePageParams(event: PageEvent) {
    const search = this.searchConfig.getValue();
    search.page = event.pageIndex;
    search.itemPerPage = event.pageSize;
    this.searchConfig.next(search);
  }

  addFabriquant(fabriquant: string) {
    const search = this.searchConfig.getValue();
    search.fabriquants.push(fabriquant);
    this.searchConfig.next(search);
  }

  addForme(forme: string) {
    const search = this.searchConfig.getValue();
    search.formes.push(forme);
    this.searchConfig.next(search);
  }

  addSubstance(substance: string) {
    const search = this.searchConfig.getValue();
    search.substances.push(substance);
    this.searchConfig.next(search);
  }

  addMedicament($event: string) {
    const search = this.searchConfig.getValue();
    search.medicament.push($event);
    this.searchConfig.next(search);
  }

  removeFabriquant(fabriquant: string) {
    const search = this.searchConfig.getValue();
    search.fabriquants = search.fabriquants.filter(f => f !== fabriquant);
    this.searchConfig.next(search);
  }

  removeForme(forme: string) {
    const search = this.searchConfig.getValue();
    search.formes = search.formes.filter(f => f !== forme);
    this.searchConfig.next(search);
  }

  removeSubstance(substance: string) {
    const search = this.searchConfig.getValue();
    search.substances = search.substances.filter(f => f !== substance);
    this.searchConfig.next(search);
  }


  removeMedicament($event: string) {
    const search = this.searchConfig.getValue();
    search.medicament = search.medicament.filter(f => f !== $event);
    this.searchConfig.next(search);
  }

  editFabriquant(fabriquant: string, newFabriquant: string) {
    const search = this.searchConfig.getValue();
    search.fabriquants = search.fabriquants.map(f => f === fabriquant ? newFabriquant : f);
    this.searchConfig.next(search);
  }

  editForme(forme: string, newForme: string) {
    const search = this.searchConfig.getValue();
    search.formes = search.formes.map(f => f === forme ? newForme : f);
    this.searchConfig.next(search);
  }

  editSubstance(substance: string, newSubstance: string) {
    const search = this.searchConfig.getValue();
    search.substances = search.substances.map(f => f === substance ? newSubstance : f);
    this.searchConfig.next(search);
  }

  editMedicament(source: string, value: string) {
    const search = this.searchConfig.getValue();
    search.medicament = search.medicament.map(f => f === source ? value : f);
    this.searchConfig.next(search);
  }

  updateFabriquantAutoComplete(event: string ) {
    return this.presentation.getFabricants(event);
  }

  updateFormeAutoComplete(event: string ) {
    return this.presentation.getFormeMed(event);
  }

  updateSubstanceAutoComplete(event: string ) {
    return this.presentation.getSubstance(event);
  }

  updateMedicamentAutoComplete($event: string) {
    return this.presentation.getMedicament($event);
  }
}

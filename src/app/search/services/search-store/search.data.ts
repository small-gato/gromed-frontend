export type sortDirection = 'ASC'|'DESC';

export type SearchField = 'fabriquants' | 'formes' | 'substances' | 'typeGens';
export enum TypeGen {
  PRINCIPS=0,
  GENERIQUE=1,
  GENERIQUE_COMPLEMENT=2,
  GENERIQUE_SUB=4,
}
export interface SearchParameters{
  page:number;
  itemPerPage:number;
  sort:string;
  direction:sortDirection;
  medicament:string[];
  fabriquants:string[];
  formes:string[];
  substances:string[];
  typeGens:TypeGen[];

}

export const emptySearch: SearchParameters = {
  page:0,
  itemPerPage:8,
  sort:'codeCip',
  direction:'ASC',
  medicament:[],
  fabriquants:[],
  formes:[],
  substances:[],
  typeGens:[],
}

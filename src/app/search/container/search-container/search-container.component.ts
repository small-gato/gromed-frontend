import { Component } from '@angular/core';
import {SearchStoreService} from "../../services/search-store/search-store.service";
import {FormControl} from "@angular/forms";
import {TypeGen} from "../../services/search-store/search.data";

export type filterType = 'fabricant'|'forme'|'substance'|'medicament';
@Component({
  selector: 'app-search-container',
  templateUrl: './search-container.component.html',
  styleUrls: ['./search-container.component.scss']
})
export class SearchContainer {
  fabriquantOptions: string[] = [];
  formeOptions: string[] = [];
  substanceOptions: string[] = [];
  medicamentOptions: string[] = [];

  groupeGenerique= new FormControl('');
  groupeGeneriqueOptions:{enum: TypeGen, label: string}[] = [
    {enum:TypeGen.GENERIQUE,label:'Générique'},
    {enum:TypeGen.GENERIQUE_COMPLEMENT,label:'Générique Complement'},
    {enum:TypeGen.GENERIQUE_SUB,label:'Générique substituable'},
    {enum:TypeGen.PRINCIPS,label:'Principes actifs'},
  ];

  constructor(private searchStore: SearchStoreService) {
    this.groupeGenerique.valueChanges.subscribe(values => searchStore.updateTypeGen(values as unknown as TypeGen[]))
  }

  get searchConfig() {
    return this.searchStore.searchConfig.asObservable();
  }

  generateTitleGroupeGenerique() {
    const value = this.groupeGenerique.value;
    if (value) {
      const label = this.groupeGeneriqueOptions.find(item => item.enum.valueOf() === Number.parseInt(value))?.label;
      return label ? label : '';
    }
    return '';
  }

  updateAutoComplete(type:filterType, $event: string) {
    switch (type) {
      case 'fabricant':
        this.searchStore.updateFabriquantAutoComplete($event).subscribe(data => this.fabriquantOptions = data);
        break;
      case 'forme':
        this.searchStore.updateFormeAutoComplete($event).subscribe(data => this.formeOptions = data);
        break;
      case 'substance':
        this.searchStore.updateSubstanceAutoComplete($event).subscribe(data => this.substanceOptions = data);
        break;
      case 'medicament':
        this.searchStore.updateMedicamentAutoComplete($event).subscribe(data => this.medicamentOptions = data);
        break;
    }
  }


  addFilter(type:filterType, $event: string) {
    switch (type){
      case 'fabricant':
        this.searchStore.addFabriquant($event);
        break;
      case 'forme':
        this.searchStore.addForme($event);
        break;
      case 'substance':
        this.searchStore.addSubstance($event);
        break;
      case 'medicament':
        this.searchStore.addMedicament($event);
        break;
    }
  }

  editFilter(type: filterType, $event: { source: string; value: string }) {
    switch (type){
      case 'fabricant':
        this.searchStore.editFabriquant($event.source, $event.value);
        break;
      case 'forme':
        this.searchStore.editForme($event.source, $event.value);
        break;
      case 'substance':
        this.searchStore.editSubstance($event.source, $event.value);
        break;
      case 'medicament':
        this.searchStore.editMedicament($event.source, $event.value);
        break;
    }
  }

  removeFilter(type: filterType, $event: string) {
    switch (type){
      case 'fabricant':
        this.searchStore.removeFabriquant($event);
        break;
      case 'forme':
        this.searchStore.removeForme($event);
        break;
      case 'substance':
        this.searchStore.removeSubstance($event);
        break;
      case 'medicament':
        this.searchStore.removeMedicament($event);
        break;
    }
  }
}

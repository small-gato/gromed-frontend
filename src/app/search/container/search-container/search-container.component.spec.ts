import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchContainer } from './search-container.component';

describe('SearchContainerComponent', () => {
  let component: SearchContainer;
  let fixture: ComponentFixture<SearchContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchContainer ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SearchContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

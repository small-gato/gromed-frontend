import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SearchContainer} from "./container/search-container/search-container.component";


const routes: Routes = [
  {
    path: '',
    component:SearchContainer
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchContainer } from './container/search-container/search-container.component';
import { DatatableComponent } from './components/datatable/datatable.component';
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatPaginatorModule} from "@angular/material/paginator";
import {SearchRoutingModule} from "./search-routing.module";
import { FilterAutoCompleteComponent } from './components/filter-auto-complete/filter-auto-complete.component';
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatChipsModule} from "@angular/material/chips";
import {MatIconModule} from "@angular/material/icon";
import {MatSelectModule} from "@angular/material/select";
import {CoreModule} from "../core/core.module";
import {MatProgressBarModule} from "@angular/material/progress-bar";



@NgModule({
  declarations: [
    SearchContainer,
    DatatableComponent,
    FilterAutoCompleteComponent
  ],
    imports: [
        CommonModule,
        MatTableModule,
        MatButtonModule,
        MatPaginatorModule,
        SearchRoutingModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatChipsModule,
        MatIconModule,
        MatSelectModule,
        CoreModule,
        MatProgressBarModule,
    ]
})
export class SearchModule { }
